import setuptools
setuptools.setup(
    scripts=['restic_exporter/restic_exporter.py'],
    author="Antonio J. Delgado",
    version='0.0.11',
    name='restic_exporter',
    author_email="",
    url="",
    description="Export to node exporter the summary of a restic backup",
    long_description="README.md",
    long_description_content_type="text/markdown",
    license="GPLv3",
    #keywords=["my", "script", "does", "things"]
)
