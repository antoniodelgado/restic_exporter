# restic_exporter

Convert the JSON output of a restic backup job to metrics that Prometheus Node Exporter would understand.
The metrics will be shown in the standard output, so then you have to direct them to a file in the
Prometheus Node Exporter folder specify with the [textfile-collector](https://github.com/prometheus/node_exporter#textfile-collector).

## Requirements

## Installation

### Linux

  ```bash
sudo python3 setup.py install
```

### Windows (from PowerShell)

  ```powershell
& $(where.exe python).split()[0] setup.py install
```

## Usage

  ```
restic_exporter.py [OPTIONS]

Options:
  -d, --debug-level [CRITICAL|ERROR|WARNING|INFO|DEBUG|NOTSET]
                                  Set the debug level for the standard output.
  -l, --log-file TEXT             File to store all debug messages.
  -j, --json-file TEXT            JSON file containing the output of restic
                                  [required]
  -n, --job-name TEXT             Restic job name to attach to the exported
                                  metrics  [required]
  -a, --extra-labels TEXT         Pairs key=value separated by commas with
                                  extra labels to add to the summary
  -m, --metric-name TEXT          Metric name. Spaces will be replaced with
                                  underscore (_).
  -d, --metric-description TEXT   Metric description.
  --config FILE                   Read configuration from FILE.
  --help                          Show this message and exit.
```

For example, you run:
```bash
restic backup --repo /var/restic_backups --json /home > /tmp/my_restic_result.json
```
Then assume your Prometheus Node Exporter is running with --collector.textfile.directory=/var/lib/node_exporter (this is set in /etc/default/prometheus-node-exporter in Ubuntu for example.
In this case of using the folder /var/lib/node_exporter, then you would run restic_exporter.py with this parameters:
```bash
restic_exporter.py --json-file /tmp/my_restic_result.json --job-name 'home_folders' > /var/lib/node_exporter/restic_home_folders.prom
```
